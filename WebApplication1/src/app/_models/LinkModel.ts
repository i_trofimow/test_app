export class LinkModel {
    constructor(
        public id: number,
        public user_id: string,
        public o_link: string,
        public s_link: string,
        public created_at: Date,
        public counter: number
    ) { }
}
