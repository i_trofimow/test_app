import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../global/global.service';
import { Router } from '@angular/router';

//this most likely should've been just inherited from index.component
@Component({
  selector: 'app-links',
  templateUrl: './links.component.html',
  styleUrls: ['./links.component.css']
})
export class LinksComponent {

    constructor(private globalService: GlobalService,
        private router: Router) { }

    getLogin() {
        return this.globalService.session_id;
    }

    login(value) {
        this.globalService.forceLogin(value);
    }

    goToLinks() {
        this.router.navigate(['/links']);
    }

    goToIndex() {
        this.router.navigate(['']);
    }

    createLink(o_link: string) {
        this.globalService.createLink(o_link);
    }

    getAllLinks() {
        return this.globalService.linksList;
    }

}
