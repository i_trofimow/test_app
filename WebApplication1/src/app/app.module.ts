import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { GlobalService } from './global/global.service';

import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CookieService } from 'ngx-cookie-service';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { LinksComponent } from './links/links.component';
import { IndexComponent } from './index/index.component';
import { FollowLinkComponent } from './follow-link/follow-link.component';

@NgModule({
  declarations: [
    AppComponent,
    LinksComponent,
    IndexComponent,
    FollowLinkComponent
  ],
  imports: [
      BrowserModule,
      FormsModule,
      HttpModule,
      AppRoutingModule
  ],
  providers: [GlobalService, CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
