import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { CookieService } from 'ngx-cookie-service';

import { LinkModel } from '../_models/LinkModel';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class GlobalService {
    session_id = 'undefined';
    sessionCookieName = 'session_id';

    linksList: LinkModel[] = [];

    constructor(private http: Http, private cookieService: CookieService) { }

    rndStr(len) {
        let text = "";
        let possible = "abcdefghijklmnopqrstuvwxyz";

        for (let i = 0; i < len; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }

    loginIfNeeded() {
        const is_logged = this.cookieService.check(this.sessionCookieName);
        if (is_logged) {
            this.session_id = this.cookieService.get(this.sessionCookieName);
        }
        else {
            this.session_id = this.rndStr(25);
            this.cookieService.set(this.sessionCookieName, this.session_id);
        }
    }

    fetchLinks() {
        this.http.get('/api/links/' + this.session_id)
            .map(result => this.linksList = result.json() as LinkModel[]).subscribe();
    }

    forceLogin(value: string) {
        this.session_id = value;
        this.cookieService.set(this.sessionCookieName, this.session_id);
        this.fetchLinks();
    }

    createLink(o_link: string)
    {
        let body = JSON.stringify({
            o_link: o_link,
        });
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        this.http.post('/api/links/' + this.session_id, body, options).subscribe(
            result => { this.linksList.push(result.json() as LinkModel); return true }
        );
    }

    updateLink(s_link: string)
    {
        return this.http.patch('/api/links/' + s_link, {})
            .map(result => { return result.text() });
    }




}
