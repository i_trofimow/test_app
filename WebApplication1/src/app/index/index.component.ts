import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../global/global.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent {

    constructor(private globalService: GlobalService,
                private router: Router) { }

    getLogin() {
        return this.globalService.session_id;
    }

    login(value) {
        this.globalService.forceLogin(value);
    }

    goToLinks() {
        this.router.navigate(['/links']);
    }

    goToIndex() {
        this.router.navigate(['']);
    }

    createLink(o_link: string) {
        this.globalService.createLink(o_link);
    }
}
