import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../global/global.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-follow-link',
  templateUrl: './follow-link.component.html',
  styleUrls: ['./follow-link.component.css']
})
export class FollowLinkComponent implements OnInit {

    constructor(private globalService: GlobalService,
                private router: Router) { }

    ngOnInit() {
        let s_link = this.router.url.split('/')[1];
        this.globalService.updateLink(s_link)
            .subscribe(result => {
                if (result != "") {
                    window.location.href = result;
                    return true;
                }
                else {
                    this.router.navigate([""]);
                }
            })
    }

}
