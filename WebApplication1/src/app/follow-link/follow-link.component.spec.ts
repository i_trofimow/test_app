import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FollowLinkComponent } from './follow-link.component';

describe('FollowLinkComponent', () => {
  let component: FollowLinkComponent;
  let fixture: ComponentFixture<FollowLinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FollowLinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FollowLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
