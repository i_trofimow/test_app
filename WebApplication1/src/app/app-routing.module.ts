import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LinksComponent } from './links/links.component';
import { IndexComponent } from './index/index.component';
import { FollowLinkComponent } from './follow-link/follow-link.component';

const routes: Routes = [
    {
        path: "",
        component: IndexComponent,
    },
    {
        path: "links",
        component: LinksComponent,
    },
    {
        path: "**",
        component: FollowLinkComponent,
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule { }
