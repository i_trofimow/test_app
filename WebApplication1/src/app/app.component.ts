import { Component, OnInit } from '@angular/core';
import { GlobalService } from './global/global.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
    title = 'app';
    constructor(private globalService: GlobalService) { }
    ngOnInit() {
        this.globalService.loginIfNeeded();
        this.globalService.fetchLinks();
    }
}
