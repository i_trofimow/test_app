using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;
using Newtonsoft.Json;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApplication1
{
    public class postLink
    {
        public string o_link { get; set; }
    }
    [Route("api/[controller]")]
    public class LinksController : Controller
    {
        // GET: api/links/user_id
        [HttpGet("{user_id}")]
        public List<Link> Get(string user_id)
        {
            DbContext context = HttpContext.RequestServices.GetService(typeof(DbContext)) as DbContext;
            return context.GetAllUserLinks(user_id);
        }

        // POST api/links/user_id
        [HttpPost("{user_id}")]
        public Link Post(string user_id, [FromBody]postLink pslink)
        {
            DbContext context = HttpContext.RequestServices.GetService(typeof(DbContext)) as DbContext;
            return context.CreateLink(user_id, pslink.o_link);
        }

        //PUT /api/links/s_link
        [HttpPatch("{s_link}")]
        public string Patch(string s_link)
        {
            DbContext context = HttpContext.RequestServices.GetService(typeof(DbContext)) as DbContext;
            return context.FollowLink(s_link);
        }
    }
}
