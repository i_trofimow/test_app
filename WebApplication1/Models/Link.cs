using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class Link
    {
        public int Id { get; set; }
        public string User_id { get; set; }
        public string O_link { get; set; }
        public string S_link { get; set; }
        public DateTime Created_at { get; set; }
        public int Counter { get; set; }
    }
}
