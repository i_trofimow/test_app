using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace WebApplication1.Models
{
  public class DbContext
  {
    private const string host_name = "http://127.0.0.1:50113/";
    private const int slink_len = 6;
    public string ConnectionString { get; set; }
    private static Random random = new Random();
    public static string RandomString(int length)
    {
      const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
      return new string(Enumerable.Repeat(chars, length)
        .Select(s => s[random.Next(s.Length)]).ToArray());
    }
    public DbContext(string connectionString)
    {
      this.ConnectionString = connectionString;
    }
    private MySqlConnection GetConnection()
    {
      return new MySqlConnection(this.ConnectionString);
    }
    public List<Link> GetAllUserLinks(string user_id)
    {
      List<Link> list = new List<Link>();
      using (MySqlConnection conn = GetConnection())
      {
        conn.Open();
        MySqlCommand cmd = new MySqlCommand(String.Format("SELECT * FROM LINKS WHERE USER_ID=\'{0}\'", user_id), conn);
        using (MySqlDataReader reader = cmd.ExecuteReader())
        {
          while(reader.Read())
          {
            list.Add(new Link()
            {
              Id = reader.GetInt32("id"),
              User_id = reader.GetString("user_id"),
              O_link = reader.GetString("o_link"),
              S_link = reader.GetString("s_link"),
              Created_at = reader.GetDateTime("created_at"),
              Counter = reader.GetInt32("counter"),
            });
          }
        }
      }
      return list;
    }
    public Link CreateLink(string user_id, string o_link)
    {
      string s_link = host_name + RandomString(slink_len);
      using (MySqlConnection conn = GetConnection())
      {
        conn.Open();
        bool needs_to_run = true;
        while (needs_to_run)
        {
          try
          {
            DateTime cur_time = DateTime.Now;
            MySqlCommand cmd = new MySqlCommand(String.Format("INSERT INTO links (user_id, o_link, s_link, created_at, counter)" +
              " VALUES (\'{0}\', \'{1}\', \'{2}\', \'{3}\', 0)",
              user_id, o_link, s_link, cur_time.ToString("yyyy-MM-dd HH':'mm':'ss")), conn);
            cmd.ExecuteNonQuery();
            needs_to_run = false;
          }
          catch
          {
            s_link = host_name + RandomString(slink_len);
          }
        }
      }
      Link created_link = null;
      using (MySqlConnection conn = GetConnection())
      {
        conn.Open();
        //well here we believe that s_link is guaranteed to be unique, altho its not unique in the db for some reason
        MySqlCommand cmd = new MySqlCommand(String.Format("SELECT * FROM links WHERE s_link=\'{0}\'", s_link), conn);
        using (MySqlDataReader reader = cmd.ExecuteReader())
        {
          while(reader.Read())
          {
            created_link = new Link()
            {
              Id = reader.GetInt32("id"),
              User_id = reader.GetString("user_id"),
              O_link = reader.GetString("o_link"),
              S_link = reader.GetString("s_link"),
              Created_at = reader.GetDateTime("created_at"),
              Counter = reader.GetInt32("counter"),
            };
          }
        }
      }
      return created_link;
    }
    public string FollowLink(string s_link)
    {
      string o_link = "";
      using (MySqlConnection conn = GetConnection())
      {
        conn.Open();
        MySqlCommand cmd = new MySqlCommand(String.Format("SELECT o_link FROM links WHERE s_link=\'{0}\'", host_name + s_link), conn);
        using (MySqlDataReader reader = cmd.ExecuteReader())
        {
          while (reader.Read())
          {
            o_link = reader.GetString("o_link");
          }
        }
        Console.WriteLine("-------------------------------notice me ------------" + o_link);
        if (o_link != "")
        {
          MySqlCommand cmd2 = new MySqlCommand(String.Format("UPDATE links SET counter = counter + 1 WHERE s_link=\'{0}\'", host_name + s_link), conn);
          cmd2.ExecuteNonQuery();
        }
      }
      return o_link;
    }
  }
}
